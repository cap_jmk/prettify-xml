# prettify-xml

[![License: GPL v3](https://img.shields.io/badge/License-GPL_v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
![Python Versions](https://img.shields.io/badge/python-3.9%20%7C%203.10%20%7C%203.11%20%7C%20-blue) 
![Style Black](https://warehouse-camo.ingress.cmh1.psfhosted.org/fbfdc7754183ecf079bc71ddeabaf88f6cbc5c00/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f636f64652532307374796c652d626c61636b2d3030303030302e737667) 
[![Documentation Status](https://readthedocs.org/projects/xml-formatter/badge/?version=latest)](https://chembee.readthedocs.io/en/latest/?badge=latest)
[![status-badge](https://ci.codeberg.org/api/badges/sail.black/chembee/status.svg)](https://ci.codeberg.org/sail.black/chembee)

# Why 

Pretty xml output is necessary for debugging xml-based communications and other operations involving XML

# What 

It is a simple command-line application formatting XML files written in Python

# Usage 

```bash
prettify-xml -i sample.xml -o formatted_test.xml
```


# Installation

## Production Build 

```bash 
pip install prettify-xml
```

## Dev Build
### Linux 

Run with one of the follwing: 
```bash
bash install.sh
./install.sh
sh install.sh
```


### Windows

Double click on `install.bat` or run

```bash
install.bat
```



# Usage 




```bash
python .\xml-format.py -i .\sample.xml -o formatted_test.xml
```

# Register a shell command 

The section is about registering shell commands using the `Click` library. Maybe you are curious, about crafting your own OS? Go for it! I do it 
and it is a lot of fun :)


The best way to have a system wide executable command on both, Windows and Linux systems is to use setuputools.

There is a description in the click package about [how to do it](https://click.palletsprojects.com/en/8.1.x/setuptools/#setuptools-integration)

In a nutshell, the setup.py file has to follow a certain pattern, e.g.

```python 
from setuptools import setup, find_packages

setup(
    name='prettify_xml',
    version='0.4.3',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': [
            'formatter = prettify_xml.formatter:format',
        ],
    },
)
```

Then, you can run on Windows: 

```bash 
python -m venv venv 
venv/Scripts/activate
pip install --editable .
```

And on Linux you can run: 

```bash 
python -m venv venv 
source venv/bin/activate
pip install --editable .
```

Then, test the application with: 

```bash
xml-formatter -i sample.xml -o formatted_test.xml
```
